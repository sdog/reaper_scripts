## sdog Reaper Project Templates

This directory structure contains Reaper project templates by sdog. 

At the moment there is only one:

1. External Stereo Router/Mixer

Help with this particular template (overview, install, usage, etc.) may be found on the [External Stereo Router/Mixer Wiki page][1]. 

Help with other Reaper software in sdog's Reaper Repository may be found on [sdog Reaper Repository Wiki Home page][2].


[1]: https://gitlab.com/sdog/reaper_scripts/-/wikis/External%20Stereo%20Mixer%20Router 
[2]: https://gitlab.com/sdog/reaper_scripts/-/wikis/home


