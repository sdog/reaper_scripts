### Starting place for Parameter Modulation Preset for Dynamic Building Stroke 

- Enable Parameter Modulation : Checked 
- Baseline Value Slider : Midway 
- Audio Control : UNchecked  
- LFO : Checked
- Shape : Triangle
- Tempo Sync : UNchecked
- Speed : 0.01Hz
- Strength : 4.0%
- Phase : 0.0
- Direction : Centered
- Phase Reset : On seek/loop
- Link from MIDI : UNchecked









