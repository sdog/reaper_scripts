##	 Binaural e-Stim Tone Generator

**Description:**  
A stereo tone generator with independently controlled channels. LFO modulators
are built in for both channels' volume parameter At least for volume, this 
works around a shortcoming in Reaper's own parameter modulation: Sometimes you
don't want to merely modulate a parameter. Sometimes you want to 
modulate/automate the modulator of the parameter \(particularly LFO parameters
like shape, frequency, strength, etc.)


**Usage:**

Although this effect is perfectly useful in the audio realm, its intended use 
is to generate stereo signals for electronic stimulation \(a.k.a. stereostim, 
a.k.a stereo e-stim, a.k.a. sending audio frequency signals to electrodes 
attached to the body). 

**SAFETY WARNING: DO NOT EVER, EVER, EVER ATTACH ELECTRODES TO YOUR BODY** 
*WITHOUT A THOROUGH UNDERSTANDING OF E-STIM SAFETY!!!*
A Google search of "e-stim safety" will get you started: 
[e-stim safety](https://www.google.com/search?q=e-stim+safety)

This generator can easily be modified as needed for more general audio 
applications. For example, the default frequency range (200-3000Hz) is a good 
range for e-stim. However, if you would like a wider frequency range for audio,
simply edit the frequency slider. 

This effect will work fine live (i.e. transport may simply sit at time=zero 
while you use the FX slider controls to create a signal in real time). 

Or you may set the transport running and use FX Automation to record 
"stimphonies" for later use in a wav/mp3 player. 

EXAMPLE APPLICATION IDEAS TO GET YOU STARTED:

Binaural Beat Generator (Audio): 
  1. Leave modulators off. 
  2. Match volume on both channels. 
  3. Choose sine shape and match frequencies on both channels.
  4. Using either the left broad frequency adjustment or the 
      right broad fequency adjustment (but not both), add or 
      subtract the binaural beat frequency you want to hear. 
      (i.e. beat frequency is the difference between L and R.) 

Tickle/Tease/Warmup Gentle Wave Generator (e-Stim): 
  1. Match volumes. 
  2. Choose sine shape, find a L frequency that feels good, 
      then match it on the R. 
  3. Leave modulators off. 
  4. Choose a pleasant wave frequency by offsetting one (and 
      only one) of the fine frequency adjustments to a nice low 
      frequency (something less than 2Hz. 0.5Hz is a nice start). 

Vibrate (e-Stim):
  1. Same settings as the Gentle Wave Generator above, but 
      slowly start pushing the frequency difference above 2Hz 
      until it feels more like vibration than wave. 

Stroke (e-stim)
  1. Choose the sine shape, match volumes and frequencies for 
      both channels. 
  2. Choose Bullet shape for both modulators.
  3. Start separating modulator timing with the Modulator 
      Sync Delay control until you feel the signal moving 
      back and forth. 

Everything else is just play and experiment!!!



