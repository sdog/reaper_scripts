/*
desc: Binaural e-Stim Tone Generator
version: 1.1
changelog: 
  v1.1 - added ability to start modulator anywhere in its cycle.
  v1.0 - initial release.
author: sdog
provides:
  sdog_Binaural_Tone_Generator.jsfx.rpl
  [rtracktemplate] sdog_Binaural_Tone_Generator_preset_modulated_building_stroke.RTrackTemplate
  [data] sdog_Binaural_Tone_Generator.reabank
link: 
  git Repository https://gitlab.com/sdog/reaper_scripts
  Help Wiki https://gitlab.com/sdog/reaper_scripts/-/wikis/Binaural%20Tone%20Generator
about:
  ##   Binaural e-Stim Tone Generator
  Requires Reaper v5.0+

  **Description:**

  A stereo tone generator with independently controlled channels. LFO modulators
  are built in for both channels' volume parameter. 

  NOTE: Not set up for full spectrum audio by default. See help wiki:
  
  You may find a fairly comprehensive help wiki with overview, install 
  intructions, and usage guidelines at:
  [Help Wiki](https://gitlab.com/sdog/reaper_scripts/-/wikis/Binaural%20Tone%20Generator)
  
*/

// Controls for the base (unmodulated) frequency: 
//
slider1:left_vol=-12<-120,0,0.1>Left Volume (dB)
// modify the upper and lower frequency limits to wider range (20-20000) to use
// this effect over the full audio range: 
slider2:left_base_freq=600<200,3000,0.1>Left Frequency (Hz) 
// useful to automate/modulate broad changes from the base frequency:
slider3:left_broad_freq_adj=0<-3000,3000,0.1>Add Left Adjustment (broad/fast)(Hz)
// useful to automate/modulate fine changes from the base frequency: 
slider4:left_fine_freq_adj=0<-20,20,0.01>Add Left Adjustment (fine/slow)(Hz)
slider5:left_shape=0<0,2,1{Sine,Triangle,Saw}>Left Shape
// 
// Controls for the LFO Volume Modulator:
// edges of discontinuous modulator shapes are ramped, except for the hard square:
slider7:left_mod_shape=0<0,6,1{None,Bullet,Sine,Triangle,Saw,Hard Square,Softened Square}>Left Volume Modulator Shape (LFO)
// skew is presently only valid for square wave: 
slider8:left_mod_skew=0.5<0,1,0.01>Left Modulator Skew (fraction of cycle)
// fractional amount to attenuate the modulator signal:
slider9:left_mod_scale=1<0,1,0.01>Left Modulator Scale 
// amount of positive shift in value for the modulator signal. Combinations of 
scale and offset that would create a modulator multiplier > 1 are not allowed:
slider10:left_mod_offset=0<0,1,0.01>Left Modulator Offset 
// speed of the LFO modulator. If set to zero, LFO rate will be tempo based. 
slider11:left_mod_rate=0<0,10.0,0.02>Left Modulator Rate (Hz) (0=tempo sync)
// Tempo rate here is measured in a fraction of a beat (e.g. in x/4 time 
// signature, 1 is a quarter note, 0.0625 is a sixteenth note.) 
slider12:left_tempo_rate=1.0<0.0625,16.0,0.0625>Left Tempo Sync (fraction of a beat)

// Right (second) channel controls are analogous to the left:
// 
slider31:right_vol=-12<-120,0,0.1>Right Volume (dB)
slider32:right_base_freq=600<200,3000,0.1>Right Frequency (Hz)
slider33:right_broad_freq_adj=0<-3000,3000,0.1>Add Right Adjustment (broad/fast)(Hz)
slider34:right_fine_freq_adj=0<-20,20,0.01>Add Right Adjustment (fine/slow)(Hz)
slider35:right_shape=0<0,2,1{Sine,Triangle,Saw}>Right Shape
// 
slider37:right_mod_shape=0<0,5,1{None,Bullet,Sine,Triangle,Saw,Hard Square,Softened Square}>Right Volume Modulator Shape (LFO)
slider38:right_mod_skew=0.5<0,1,0.01>Right Modulator Skew (fraction of cycle)
slider39:right_mod_scale=1<0,1,0.01>Right Modulator Scale 
slider40:right_mod_offset=0<0,1,0.01>Right Modulator Offset 
slider41:right_mod_rate=0<0,10.0,0.02>Right Modulator Rate (Hz) (0=tempo sync)
slider42:right_tempo_rate=1.0<0.0625,16.0,0.0625>Right Tempo Sync (fraction of a beat)

// fraction of a modulator cycle that the left channel will lag the right:
slider50:mod_sync_delay=0<-0.5,0.5,0.01>Modulator Sync Delay (fraction of cycle)

// Strictly an output synth. Not intended to mix input: 
in_pin:none
out_pin:left output
out_pin:right output

@init
//aadbg_init_calls+=1;
///////////////////////////////////////////////////////////////////////////////
// CONSTANT Definitions. Many of these are based on subjective feel and may 
// be modified by the script-and-math savvy user (with appropriate forethought 
// and testing).  
//

// Fractional amount amplitude may step before we smooth the discontinuity: 
AMP_RAMP_THRESHOLD=0.05; 

// Fractional amount frequency may step before we smooth the discontinuity: 
FREQ_RAMP_THRESHOLD=0.25; 

// Our best guess at the number of cycles it might take to create a smooth 
// transition:
CYCLES_PER_TRANSITION=8; 

// Lower limit to the audio frequency in Hz. Lower frequencies do not feel 
// good for e-stim, but this value can be set lower (e.g. 20Hz) for pure 
// audio usage: 
MIN_AUDIO_FREQ=100; 

// Number of seconds we're willing to wait for the end of a cycle to 
// transition (fractions are O.K. - i.e. 0.5 for 500 milliseconds): 
// Transitions between certain modulator control values can be made smoother by
// waiting until the end of a modulator cycle, but for a slower LFO frequency, 
// this may take longer than we are willing to wait. In that case we will 
// transition with a simple fade out and back in. 
MOD_CYCLE_WAIT_THRESHOLD=1;

// Root used to determine bullet shape (steepness). (e.g. 2=square root, 
// 3=cube root, etc.):
ROOT=2; 

// Limit modulator rate to this number of tone cycles per modulator cycle
// (This is to prevent modulator from going so fast that it becomes meaningless
// as a modulator.): 
MIN_CYCLES_PER_MOD_CYCLE=8;

// By default, the left modulator cycle will start at the zero point before
// the rising slope of the modulator shape. Change this setting to start 
// the modulator cycle at any point along its shape. Not particularly useful
// except for tempo/beat based modulators where the modulator cycle coincides
// with beat.
// Valid range is [0,1). 
MOD_CYCLE_START_SHIFT=0.0;

///////////////////////////////////////////////////////////////////////////////
// Function declarations here: 
//
function normalize_cycle(p) (
  // For a cycle whose position is measured within a unit (i.e. position ranges
  // from [0,1) vs. a position measured in radians (ranging from [0,2*pi])), 
  // this function will return the given cycle position p, normalized to a 
  // value within range. 
  p-=floor(p);
);

function get_mod_adjustment(mod_rate, tempo_rate) local(mod_adj) (
  // Return the distance between samples for the modulator cycle, measured 
  // as the fraction of a cycle per sample, based on the given modulator rate 
  // and tempo rate. 
  //
  // NOTE: Undefined when called outside of @block or @sample because of the 
  // limited scope of the "tempo" variable. 
  (mod_rate == 0) ? ( // tempo based modulator)
    mod_adj = (tempo/(60.0*tempo_rate))/srate;
  ):(                 // freq based modulator
    mod_adj = mod_rate/srate;
  );
  mod_adj;
);

function get_mod(shape pos skew scale offset) (
  /* Return the modulation multiplier value for the sample at the given 
    position based on the chosen shape, skew, scale, and offset of the 
    modulator. 
    
    The modulator's amplitude will modulate between 0 and 1 (as opposed to 
    the underlying audio signal, traversing from -1 to 1). 
  */
  // none:
  (shape == 0) ? (  
    mult = 1; 
  ):
  // bullet:
  (shape == 1) ? (  
    mult = pow(pos,(1.0/ROOT)); 
    (pos <= 0.02) ? (  // a little slope at the beginning to smooth discontinuity
      mult *= (50*pos)^2;
    );
    (pos >= 0.98) ? (  // a little slope at the end to smooth discontinuity
      mult *= (50*(1.0-pos))^2.0;
    );
  ):
  // sine:
  (shape == 2) ? (  
    mult = .5 + sin(2*$pi*pos-$pi/2)/2;
  ): 
  // triangle:
  (shape == 3) ? (  
    (pos <= 0.5) ? (
      mult = 2*pos;
    ):(
      mult = 2*(1-pos);
    );
  ): 
  // saw:
  (shape == 4) ? (  
    mult = pos;
    (pos >= .98) ? ( // a little slope at the end to smooth discontinuity
      mult *= (50*(1.0-pos))^2.0;
    );
  ):  
  // hard square:
  (shape == 5) ? (  
    mult = (pos >= skew); 
  ): 
  // softened square (soften discontinuities):
  (shape == 6) ? (  
    mult = (pos >= skew); 
    ((pos-skew) >=0 && (pos-skew) <=0.02) ? ( // a little slope at the beginning to smooth discontinuity
        mult *= (50*(pos-skew))^2.0;  
    );
    (pos >= 0.98) ? (  // a little slope at the end to smooth discontinuity
      mult *= (50*(1.0-pos))^2.0;
    );
  ): 
  // undefined:
  (
    mult=0;
  ); 

  mult = mult * scale + offset;
);

function get_audio_tone(shape pos) local (v)  (
  // Return the value for the sample at cycle position pos for the given shape
  // (to create an unmodulated steady state audio tone (-1,1)).

  // sine:
  (shape == 0) ? ( 
    v = sin(2*$pi*pos);
  ) : 
  // triangle:
  (shape == 1) ? ( 
    // TODO: I'm sure there's a more elegant way to do a triangle, but only 
    // after a fresh cup of coffee. (want to start the upslope at t=zero.) 
    (pos <= 0.25) ? (v = 4.0*pos;) :
    (pos > 0.25 && pos <= 0.75) ? (v = 2.0 - 4.0*pos; ) :
    v = 4.0*pos - 4.0;
  ) : 
  // saw:
  (shape == 2) ? (          
    v = 1.0-2.0*pos;
  ) : 
  // undefined:
  (   
    v=0;
  );
  v;
);

function advance(p, f) ( 
  // Advance the position in a cycle measured in units ([0,1) vs. radians) from 
  // position p by fraction of cycle f. 
  p+=f;
  p=normalize_cycle(p);
);

// initialize a few variables with defaults before the first slider move:
freq0=left_base_freq+left_broad_freq_adj+left_fine_freq_adj;
freq1=right_base_freq+right_broad_freq_adj+right_fine_freq_adj;
adj0=freq0/srate;
adj1=freq1/srate;
samples_per_ramp0=ceil(CYCLES_PER_TRANSITION/adj0);
samples_per_ramp1=ceil(CYCLES_PER_TRANSITION/adj1);
mod_pos0=MOD_CYCLE_START_SHIFT;


@slider
//aadbg_slider_calls+=1;
//aadbg_slider_tempo=tempo;

  /////////////////////////////////////////////////////////////////////////////
  // Disallow section (disallow certain values for certain sliders):
  //
  // Keep offset controls in range (keep overall amplitude below 1 and keep
  // overall frequency above lower limit). 
  left_mod_offset = min(left_mod_offset,1-left_mod_scale);
  right_mod_offset = min(right_mod_offset,1-right_mod_scale);
  left_broad_freq_adj = max(left_broad_freq_adj, MIN_AUDIO_FREQ-left_base_freq);
  right_broad_freq_adj = max(right_broad_freq_adj, MIN_AUDIO_FREQ-right_base_freq);
  left_fine_freq_adj = max(left_fine_freq_adj, MIN_AUDIO_FREQ-(left_base_freq+left_broad_freq_adj));
  right_fine_freq_adj = max(right_fine_freq_adj, MIN_AUDIO_FREQ-(right_base_freq+right_broad_freq_adj));

  // Target values (where we are heading) are updated every time a control 
  // changes. Working values change only according to specific rules 
  // of transition. 
  
  // convert volume to amplitude. 
  target_amp0 = 2^(left_vol/6); 
  target_amp1 = 2^(right_vol/6); 

  // actual frequency after adjustments
  target_freq0=left_base_freq+left_broad_freq_adj+left_fine_freq_adj;
  target_freq1=right_base_freq+right_broad_freq_adj+right_fine_freq_adj;

  // fraction of cycle per sample
  target_adj0=target_freq0/srate;
  target_adj1=target_freq1/srate;

  target_shape0=floor(abs(left_shape));
  target_shape1=floor(abs(right_shape));

  target_mod_shape0=floor(abs(left_mod_shape));
  target_mod_shape1=floor(abs(right_mod_shape));
  target_mod_skew0=left_mod_skew;
  target_mod_skew1=right_mod_skew;
  target_mod_scale0=left_mod_scale;
  target_mod_scale1=right_mod_scale;
  target_mod_offset0=left_mod_offset;
  target_mod_offset1=right_mod_offset;

  /* // sliders that interact with tempo must be polled in @sample or @block. 
  target_mod_rate0=left_mod_rate;
  target_mod_rate1=right_mod_rate;
  target_tempo_rate0=left_tempo_rate;
  target_tempo_rate1=right_tempo_rate;
  */

  // The number of samples we want to traverse while smoothing discontinuities. 
  samples_per_ramp0=ceil(CYCLES_PER_TRANSITION/min(adj0,target_adj0));
  samples_per_ramp1=ceil(CYCLES_PER_TRANSITION/min(adj1,target_adj1));

  // TODO: Setting these for now. Don't really set working vars unless they 
  // pass transition tests. But because we've only done skew for square and 
  // square is discontinuous anyway, just change it now. 
  mod_skew0=target_mod_skew0;
  mod_skew1=target_mod_skew1;

  // calculate any transition needs. If there are no transition needs, 
  // update working values with target values.  
  needs_L_adj_ramp = (abs((target_adj0-adj0)/adj0) > FREQ_RAMP_THRESHOLD) * samples_per_ramp0;
  (needs_L_adj_ramp) ? adj0_inc=(target_adj0-adj0)/samples_per_ramp0
    : adj0=target_adj0; 
 
  needs_R_adj_ramp = (abs((target_adj1-adj1)/adj1) > FREQ_RAMP_THRESHOLD) * samples_per_ramp1;
  (needs_R_adj_ramp) ? adj1_inc=(target_adj1-adj1)/samples_per_ramp1 // distance of step.
    : adj1=target_adj1; 
  
  needs_cycle_resync=(target_adj0===target_adj1 && pos0 != pos1 ) * ceil(CYCLES_PER_TRANSITION/target_adj0);
  (needs_cycle_resync) ? first_resync=1;

  needs_L_amp_ramp=(abs(target_amp0-amp0) > AMP_RAMP_THRESHOLD) * samples_per_ramp0; 
  (needs_L_amp_ramp) ? amp0_inc=(target_amp0-amp0)/samples_per_ramp0
    : amp0=target_amp0;
  
  needs_R_amp_ramp=(abs(target_amp1-amp1) > AMP_RAMP_THRESHOLD) * samples_per_ramp1; 
  (needs_R_amp_ramp) ? amp1_inc=(target_amp1-amp1)/samples_per_ramp1
    : amp1=target_amp1;

  needs_L_mod_scale_ramp=(abs(target_mod_scale0-mod_scale0) > AMP_RAMP_THRESHOLD) * samples_per_ramp0; 
  (needs_L_mod_scale_ramp) ? mod_scale0_inc=(target_mod_scale0-mod_scale0)/samples_per_ramp0
    : mod_scale0=target_mod_scale0;
  
  needs_R_mod_scale_ramp=(abs(target_mod_scale1-mod_scale1) > AMP_RAMP_THRESHOLD) * samples_per_ramp1; 
  (needs_R_mod_scale_ramp) ? mod_scale1_inc=(target_mod_scale1-mod_scale1)/samples_per_ramp1
    : mod_scale1=target_mod_scale1;
  
  needs_L_mod_offset_ramp=(abs(target_mod_offset0-mod_offset0) > AMP_RAMP_THRESHOLD) * samples_per_ramp0; 
  (needs_L_mod_offset_ramp) ? mod_offset0_inc=(target_mod_offset0-mod_offset0)/samples_per_ramp0
    : mod_offset0=target_mod_offset0;
  
  needs_R_mod_offset_ramp=(abs(target_mod_offset1-mod_offset1) > AMP_RAMP_THRESHOLD) * samples_per_ramp1; 
  (needs_R_mod_offset_ramp) ? mod_offset1_inc=(target_mod_offset1-mod_offset1)/samples_per_ramp1
    : mod_offset1=target_mod_offset1;
  
  (target_mod_shape0 != mod_shape0) ? (
    mod_cycle_remaining0=((1-mod_pos0)/mod_adj0)/srate;
    (mod_cycle_remaining0 < MOD_CYCLE_WAIT_THRESHOLD && 
     target_mod_shape0 && mod_shape0) ? // if either is "none", we'll have to fade instead of wait.  
      needs_L_mod_shape_wait=1
      : needs_L_mod_shape_fade=1;
  );

  (target_mod_shape1 != mod_shape1) ? (
    mod_cycle_remaining1=((1-mod_pos1)/mod_adj1)/srate;
    (mod_cycle_remaining1 < MOD_CYCLE_WAIT_THRESHOLD && 
     target_mod_shape1 && mod_shape1) ? // if either is "none", we'll have to fade instead of wait.  
      needs_R_mod_shape_wait=1
      : needs_R_mod_shape_fade=1;
  );

  (!fading0 && needs_L_mod_shape_fade) ? (
    needs_L_mod_shape_fade=0; // don't start a new fade unless a slider asks 
    fading0=1;
    samples_per_mod_fade0=samples_per_ramp0;
    needs_L_mod_fade = 2.0 * samples_per_mod_fade0;
  );

  (!fading1 && (needs_R_mod_shape_fade)) ? (
    needs_R_mod_shape_fade=0; // don't start a new fade unless a slider asks 
    fading1=1;
    samples_per_mod_fade1=samples_per_ramp1;
    needs_R_mod_fade= 2.0 * samples_per_mod_fade1;
  );  


@block
//aadbg_block_calls+=1;

// Since the JSFX "tempo" variable is not valid outside the @block and @sample
// sections, logic for any aspect of the signal's control that relies on tempo
// must be worked out here, rather than in the slider section. This includes 
// polling any slider controls that interact with tempo. For now, this is 
// mostly anything to do with modulator timing. 
target_mod_adj0=get_mod_adjustment(left_mod_rate, left_tempo_rate);
target_mod_adj1=get_mod_adjustment(right_mod_rate, right_tempo_rate);
// Disallow a super fast modulator: 
target_mod_adj0=min(target_mod_adj0,adj0/MIN_CYCLES_PER_MOD_CYCLE);
target_mod_adj1=min(target_mod_adj1,adj1/MIN_CYCLES_PER_MOD_CYCLE);

// sync delay parameter is ignored unless it is meaningful. It is only 
// meaningful if both modulators are at the same rate. 
// NOTE: It could also be meaningful or interesting if the two modulator rates
// were in an integer relationship with each other, (i.e. if their interaction
// created a recognizable repeating pattern), but that logic/math is for 
// another day.
respect_mod_sync=
  (left_mod_shape && right_mod_shape) // don't bother for modulation is none. 
  && (target_mod_adj0===target_mod_adj1); // don't bother if the frequencies 
                                          // aren't equal. 

next_mod_pos1=advance(mod_pos1,target_mod_adj1);
target_mod_pos1=advance(mod_pos0+mod_sync_delay,target_mod_adj1);
out_of_sync=(next_mod_pos1 != target_mod_pos1);

needs_mod_resync=respect_mod_sync && out_of_sync;

// TODO: Based on early testing, I think modulator frequency changes created 
// minimal discontinuities, so we can probably simply jump to the new rates. 
needs_L_mod_adj_chg=(mod_adj0 !== target_mod_adj0);
needs_R_mod_adj_chg=(mod_adj1 !== target_mod_adj1);
mod_adj0 = target_mod_adj0;
mod_adj1 = target_mod_adj1;

(needs_mod_resync) ? (
  (!fading1) ? (
    fading1=1;
    samples_per_mod_fade1=samples_per_ramp1;
    needs_R_mod_fade = 2.0 * samples_per_mod_fade1;
  );
);  

@sample
//aadbg_sample_calls+=1;

// unless we are in the middle of a transition, modulator multiplier should be 
// unity. 
mod_mult0=1;
mod_mult1=1;

(needs_cycle_resync) ? (
  (!needs_L_adj_ramp && !needs_R_adj_ramp) ? (
    // adj positions slightly to bring closer in sync, but  
    // only if we're not still adjusting the frequencies. 
    (first_resync) ? (
      // first time through, set the position increment. 
      pos_inc=(pos1-pos0)/needs_cycle_resync; 
      first_resync=0;
    );
    pos0+=pos_inc;
    needs_cycle_resync-=1;
  ); // end position adjustment.  
); // end needs_cycle_resync

(needs_L_adj_ramp) ? (
  adj0 += adj0_inc;
  needs_L_adj_ramp-=1;
);

(needs_R_adj_ramp) ? (
  adj1 += adj1_inc;
  needs_R_adj_ramp-=1;
);

(needs_L_amp_ramp) ? (
  amp0 += amp0_inc;
  needs_L_amp_ramp-=1;
);

(needs_R_amp_ramp) ? (
  amp1 += amp1_inc;
  needs_R_amp_ramp-=1;
);

(needs_L_mod_scale_ramp) ? (
  mod_scale0 += mod_scale0_inc;
  needs_L_mod_scale_ramp-=1;
);

(needs_R_mod_scale_ramp) ? (
  mod_scale1 += mod_scale1_inc;
  needs_R_mod_scale_ramp-=1;
);

(needs_L_mod_offset_ramp) ? (
  mod_offset0 += mod_offset0_inc;
  needs_L_mod_offset_ramp-=1;
);

(needs_R_mod_offset_ramp) ? (
  mod_offset1 += mod_offset1_inc;
  needs_R_mod_offset_ramp-=1;
);

(needs_L_mod_shape_wait) ? ( 
  ((mod_pos0+mod_adj0) > 1 ) ? (
    mod_shape0=target_mod_shape0;
    needs_L_mod_shape_wait=0;
  );
);

(needs_R_mod_shape_wait) ? ( 
  ((mod_pos1+mod_adj1) > 1) ? (
    mod_shape1=target_mod_shape1;
    needs_R_mod_shape_wait=0;
  );
);

(needs_L_mod_fade) ? (
  (needs_L_mod_fade==samples_per_mod_fade0) ? (
    // if we're at the bottom of the fade, update. 
    fading0=0;
    mod_shape0=target_mod_shape0;
    // don't set modulator position here, set it in R channel if delay is needed. 
  );
  (needs_L_mod_fade>samples_per_mod_fade0) ? ( // fade out
    mod_mult0 = (needs_L_mod_fade-samples_per_mod_fade0)/samples_per_mod_fade0;
  ):(                   // fade back in 
    mod_mult0 = (samples_per_mod_fade0-needs_L_mod_fade)/samples_per_mod_fade0;
  );    
  needs_L_mod_fade-=1;
); // end needs left mod fade

(needs_R_mod_fade) ? (
  (needs_R_mod_fade==samples_per_mod_fade1) ? (
    // if we're at the bottom of the fade, update. 
    fading1=0;
    mod_shape1=target_mod_shape1;
    (needs_mod_resync) ?  (
      mod_pos1=normalize_cycle(mod_pos0+mod_sync_delay);
      needs_mod_resync=0;
    );
  );
  (needs_R_mod_fade>samples_per_mod_fade1) ? ( // fade out
    mod_mult1 = (needs_R_mod_fade-samples_per_mod_fade1)/samples_per_mod_fade1;
  ):(                   // fade back in 
    mod_mult1 = (samples_per_mod_fade1-needs_R_mod_fade)/samples_per_mod_fade1;
  );    
  needs_R_mod_fade-=1;
); 


///////////////////////////////////////////////////////////////////////////////
// No matter what happens in transition, we will need a sample calculated from 
// the current working values and we will need the sample position advanced by 
// the working amount.
// Any major discontinuities remaining at this point are due to flaws in our 
// logic above as we attempt to transition gently. 
//

// get sample value for the un-modulated audio tone:
tone0 = get_audio_tone(shape0, pos0);
tone1 = get_audio_tone(shape1, pos1);

// calculate modulation factor according to chosen shape:
mod0 = get_mod(mod_shape0, mod_pos0, mod_skew0, mod_scale0, mod_offset0);
mod1 = get_mod(mod_shape1, mod_pos1, mod_skew1, mod_scale1, mod_offset1);

// multiply working sample value by all relevant factors:
tone0 *= amp0 * mod0 * mod_mult0;
tone1 *= amp1 * mod1 * mod_mult1;

// By design, sample values should never be over 1. If they are, we have  
// some unknown programming error with unknown consequences. Kill the output 
// until the effect is reset. 
(tone0>=1 || tone1>=1) ? aaSAFETY_BRAKE+=1; 
(aaSAFETY_BRAKE) ? tone0=tone1=0;

// set the samples:
spl0 = tone0;
spl1 = tone1;

// and move to the next sample position in all cycles:
mod_pos0=advance(mod_pos0, mod_adj0);
mod_pos1=advance(mod_pos1, mod_adj1); 
pos0=advance(pos0, adj0);
pos1=advance(pos1, adj1);


