## sdog JSFX Synth Scripts

This directory structure contains Reaper JSFX Synth scripts by sdog. 

At the moment there are only two:

1. Binaural e-Stim Tone Generator
2. Binaural e-Stim Tone Generator v2

Help with this particular script (overview, install, usage, etc.) may be found on the [Binaural Tone Generator Wiki page][1]. 

Help with other software in sdog's Reaper Repository may be found on [sdog Reaper Repository Wiki Home page][2].


[1]: https://gitlab.com/sdog/reaper_scripts/-/wikis/Binaural%20Tone%20Generator 
[2]: https://gitlab.com/sdog/reaper_scripts/-/wikis/home


