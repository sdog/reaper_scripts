## Main sdog Reaper Repository

This repository contains various scripts and other goodies by user sdog 
for use with the Cockos Reaper DAW. 

Most can be installed with cfillion's excellent [ReaPack][1] software. 

Add this URL:

`https://gitlab.com/sdog/reaper_scripts/raw/main/index.xml`

to ReaPack's Repository Manager, using the instructions in the 
[Reapack User Guide][2]. 

As of this writing, this repository contains the following packages:

1. Binaural e-Stim Tone Generator (JSFX + Support files) 
2. Binaural e-Stim Tone Generator v2 (JSFX + Support files) 
3. External Stereo Router/Mixer (Reaper Project Template)

More details about the software in this repository (overview, install help, usage, etc.) can be found on the [Main sdog Reaper Repository Wiki][3].


[1]: https://reapack.com/
[2]: https://reapack.com/user-guide
[3]: https://gitlab.com/sdog/reaper_scripts/-/wikis/home
